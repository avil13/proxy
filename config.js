module.exports = [

    // dev
    {
        _path_: 'services/contract-template',
        path: ['services/contract-template/', ''],
        port: 9094
    },
{
    _path_: 'fake-contracts',
    port: 9094
},
    {
        _path_: 'services',
        port: 9094
    },
    {
        _path_: 'login',
        port: 9094
    },
    {
        _path_: 'logout',
        port: 9094
    },

    {
        _path_: '/back-end',
        // hostname: '132055.simplecloud.club',
        // hostname: '192.168.10.10',
        hostname: 'sber.loc',
        path: ["/back-end", ""],
        port: 80
    },
    {
        _path: '/rq',
        hostname: 'lab.swew.loc',
        port: 80
    },
    {
        _path_: '/api',
        // hostname: '132055.simplecloud.club',
        // hostname: '192.168.10.10',
        hostname: 'sber.loc',
        // path: ["/back-end", "/api"],
        path: ["/api", ""],
        port: 80

    },
    {
        _path_: '',
        hostname: 'localhost',
        port: 4200
    }
];
